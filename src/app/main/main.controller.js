(function() {
    'use strict';

    angular.module('dhub_sso').controller('MainController', ['Auth', '$scope', MainController]);

    function MainController(Auth, $scope) {
        $scope.logout = Auth.logout;

        $scope.isLoggedIn = function() {
            return Auth.authenticated;
        };
    }
})();
