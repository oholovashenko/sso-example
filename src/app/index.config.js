(function() {
    'use strict';

    angular.module('dhub_sso').config(['$httpProvider','toastrConfig',
        function ($httpProvider, toastrConfig) {
            $httpProvider.interceptors.push('errorInterceptor');
            $httpProvider.interceptors.push('authInterceptor');

            angular.extend(toastrConfig, {
                timeOut: 3000,
                progressBar: true,
                allowHtml: true,
                positionClass: 'toast-top-center'
            });
        }]);

})();
