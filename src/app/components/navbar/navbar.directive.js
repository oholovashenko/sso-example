(function() {
  'use strict';

  angular.module('dhub_sso').directive('exampleNavbar', function () {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',

      controller: ['Auth', '$scope', function (Auth, $scope) {
        $scope.logout = Auth.logout;
        $scope.isLoggedIn = Auth.authenticated;
      }],

      controllerAs: 'vm',
      bindToController: true
    };

    return directive;
  });
})();
