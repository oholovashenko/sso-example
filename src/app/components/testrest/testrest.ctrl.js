(function() {
    'use strict';

    angular.module('dhub_sso.testrest')
        .controller('RESTCtrl', [ '$scope', 'WeightData', 'Auth', function($scope, WeightData, Auth) {
            
            function getWeights() {
                WeightData.all().then(function (result) {
                    $scope.weights = result.data.objects;
                    console.log($scope.weights);
                }, function (result) {
                    console.log(result.status);
                    console.log(result.data);
                });
            }

            $scope.weights = [];
            getWeights();

            $scope.request_header = Auth.token;

        }]);
})();
