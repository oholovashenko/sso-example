(function() {
    'use strict';

    angular.module('dhub_sso.testrest').config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/testrest', {
            templateUrl: 'app/components/testrest/datahub.html',
            controller: 'RESTCtrl'
        });
    }]);

})();


