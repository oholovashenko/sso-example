(function() {
  'use strict';

    angular.module('dhub_sso.testrest').constant('ENDPOINT_URI', 'http://137.135.90.65:8010/healthinfo/api/v1/')
        .service('WeightData', [ '$http', 'ENDPOINT_URI', weightData]);

    function weightData($http, ENDPOINT_URI) {
        var service = this;
        var path = 'weight/';

        function getUrl() {
            return ENDPOINT_URI + path;
        }

        service.all = function() {
            return $http.get(getUrl());
        };

    }
})();

