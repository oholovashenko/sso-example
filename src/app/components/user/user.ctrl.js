(function() {
  'use strict';

  angular.module('dhub_sso.user').controller('UserCtrl', [ '$rootScope','$scope', 'Auth', function ($rootScope, $scope, Auth) {
        if (!Auth.authenticated) {
            $rootScope.$broadcast('event:auth-loginRequired');
        }
        $scope.authinfo = Auth;

        $scope.isTeam = function() {
            return Auth.hasRealmRole('team');
        };
    }]);

})();
