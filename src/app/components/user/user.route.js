(function() {
    'use strict';

    angular.module('dhub_sso.user').config(['$routeProvider',function ($routeProvider) {
        $routeProvider.when('/user', {
            templateUrl: 'app/components/user/user.html',
            controller: 'UserCtrl'
        });
    }]);

})();
