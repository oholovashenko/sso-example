(function() {
    'use strict';

    angular
        .module('dhub_sso', [
            'angular-loading-bar',
            'ngAnimate',
            'ngSanitize',
            'ngRoute',
            'toastr',
            'ui.bootstrap',
            'dhub_sso.user',
            'dhub_sso.testrest'
        ]);

})();
