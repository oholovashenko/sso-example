(function() {
    'use strict';

    angular.module('dhub_sso').config(['$routeProvider', routeConfig]);

    function routeConfig($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'app/main/main.html',
                controller: 'MainController'
            })
            .otherwise({redirectTo: '/'});
    }

})();
