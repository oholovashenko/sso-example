(function() {
    'use strict';

    angular.element(document).ready(function () {
        var keycloakAuth = new Keycloak('app/keycloak.json');

        keycloakAuth.init({ onLoad: 'check-sso' }).success(function () {
            angular
                .module('dhub_sso')
                .factory('Auth', function() {
                    return keycloakAuth;
                });
            angular.bootstrap(document, ["dhub_sso"]);
        }).error(function () {
                window.location.reload();
            });

    });

    angular.module('dhub_sso').run(['$rootScope', '$location', 'Auth', runBlock]);

    function runBlock($rootScope, $location, Auth) {
        $rootScope.$on( "event:auth-loginRequired", function() {
            Auth.login();
        });
    }

})();
